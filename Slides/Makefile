# All hope abandon ye who enter here
MAKEFLAGS += -r -R -j $(shell getconf _NPROCESSORS_ONLN)

TEXINPUTS := .:$(CURDIR):$(CURDIR)/common:
export TEXINPUTS

-include Makefile.conf

# don not cleanup build output ...
.SECONDARY:

.SILENT :

.DEFAULT_GOAL := all

CURDIRBASE := $(notdir $(CURDIR))
RELEASE_FILENAME := $(CURDIRBASE).tar.gz

FIGUREDIR := figures

BUILDDIR := build

FIGURETEXSRC := $(wildcard $(FIGUREDIR)/*.tex) $(wildcard $(FIGUREDIR)/*/*.tex)
FIGURETEXPDF := $(FIGURETEXSRC:.tex=.pdf)
FIGURETIKZSRC := $(wildcard $(FIGUREDIR)/*.tikz) $(wildcard $(FIGUREDIR)/*/*.tikz)
FIGURETIKZPDF := $(filter-out $(FIGURETEXPDF),$(FIGURETIKZSRC:.tikz=.pdf))
FIGURESRC := $(FIGURETEXSRC) $(FIGURETIKZSRC)
FIGUREPDF := $(FIGURETEXPDF) $(FIGURETIKZPDF)
DEPS      := $(MAKEFILE_LIST)

# studentdefs.tex itself does not build a pdf
TEXSRC := $(filter-out $(TEXSKIP),$(wildcard *.tex))
TEXPDF := $(TEXSRC:.tex=.pdf)
# allow building protocol instead of protocol.pdf
TEXSTEM := $(TEXSRC:.tex=)

TEXSLIDESRC := $(wildcard slides*.tex)
#TEXSLIDENOTEPDF := $(TEXSLIDESRC:.tex=-notes.pdf)

# only include (re-)buildable dependencies
# extend when adding new _$(JOBNAME) targets
DEPSMAKE := $(TEXPDF:%=$(BUILDDIR)/%.d) $(TEXSLIDENOTEPDF:%=$(BUILDDIR)/%.d)
DEPSMAKEEXIST := $(wildcard $(DEPSMAKE))
DEPSFIGUREMAKE := $(FIGUREPDF:%=$(BUILDDIR)/%.d)
DEPSFIGUREMAKEEXIST := $(wildcard $(DEPSFIGUREMAKE))

# since empty include targets are not remade: https://www.gnu.org/software/make/manual/html_node/Remaking-Makefiles.html#Remaking-Makefiles
# create empty dependency includes
$(BUILDDIR)/%.d: MAGIC
	mkdir -p $(dir $@)
	touch $@

.PHONY: MAGIC
MAGIC:

# include dependency makefiles, but only if they exist (wildcard LIST)
# in order to force creation/update if *.d was deleted
$(DEPSMAKEEXIST): ;
include $(DEPSMAKEEXIST)

$(DEPSFIGUREMAKEEXIST): ;
include $(DEPSFIGUREMAKEEXIST)

PDFLATEX ?= pdflatex
BIBTEX ?= bibtex
DIFF ?= diff
GREP ?= grep
AWK ?= awk
LS ?= ls
MV ?= mv
SED ?= sed
UNIQ ?= uniq
SORT ?= sort
FIND ?= find
TAR ?= tar

PDFLATEX_FLAGS	:= -interaction=batchmode -recorder -file-line-error -shell-escape
GREP_FLAGS += -E -B 2 -A 3

# terminal colors
ifneq ($(COLORTERM),false)
  NOCOLOR := "\033[0m"
  RED := "\033[1;31m"
  BLUE := "\033[1;34m"
  GREEN := "\033[1;32m"
  YELLOW := "\033[1;33m"
  CYAN := "\033[1;36m"
  WHITE := "\033[1;37m"
  MAGENTA := "\033[1;35m"
  BOLD := "\033[1m"
else
  NOCOLOR := ""
  RED := ""
  BLUE := ""
  GREEN := ""
  YELLOW := ""
  CYAN := ""
  WHITE := ""
  MAGENTA := ""
  BOLD := ""
endif

# helper functions for filename conversion
getaux = $(basename $(1)).aux

# messaging functions
msgtarget = printf $(GREEN)"%s"$(MAGENTA)" %s"$(NOCOLOR)"\n" "$(1)" "$(2)"
msgcompile = printf $(BOLD)"%-25s"$(NOCOLOR)" %s\n" "[$(1)]" "$(2)$(if $(3), -jobname $(3))"
msgfail = printf "%-25s "$(RED)"%s"$(NOCOLOR)"\n" "" "FAILED! Continuing ..."
msginfo = printf "%-25s "$(CYAN)"%s"$(NOCOLOR)"\n" "" "$(1)"

# $(1) document
# $(2) jobname
# $(3) output-directory
define run-typeset
  $(call msgcompile,$(PDFLATEX),$(1),$(2)); \
  cd $(3) && $(PDFLATEX) $(PDFLATEX_FLAGS) $(if $(2),-jobname=$(2)) $(1) </dev/null >/dev/null 2>&1 || \
    $(call msgfail)
endef

define run-draft-typeset
  $(call msgcompile,$(PDFLATEX),$(1),$(2)); \
  cd $(3) && $(PDFLATEX) $(PDFLATEX_FLAGS) $(if $(2),-jobname=$(2)) -draftmode $(1) </dev/null >/dev/null 2>&1 || \
    $(call msgfail)
endef

define run-bibtex
  $(call msgcompile,$(BIBTEX),$(call getaux,$(1))); \
  $(BIBTEX) $(BIB_FLAGS) $(call getaux,$(1)) 1>/dev/null 2>&1 || \
    $(call msgfail)
endef

define grep-citation
  $(GREP) $(GREP_FLAGS) -e "Warning: Citation .*" \
    $(call getlog,$(1)) 1>/dev/null 2>&1
endef

define check-citation
  $(GREP) -e'^\\citation' $(call getaux,$(1)) 2>/dev/null \
    >$(call gettemp,$(1)); \
  $(DIFF) $(call gettemp,$(1)) $(call getcit,$(1)) 1>/dev/null 2>&1 || \
        (mv -f $(call gettemp,$(1)) $(call getcit,$(1)); false)
endef

define grep-crossref
  $(GREP) $(GREP_FLAGS) -e "Rerun to get .*" \
    $(call getlog,$(1)) 1>/dev/null 2>&1 && \
    $(call msginfo,Rerun latex to get everything right.)
endef

define extract-log
  $(call msgtarget,Extracting log file for target,$(1)); \
  $(GREP) -E -v -e "^<Error-correction level increased from . to . at no cost\\.>$$" $(1).log | \
  $(GREP) $(GREP_FLAGS) -e ":[[:digit:]]+: |Warning|Error|Underfull|Overfull|\!|Reference|Label|Citation" || :
endef

# based on https://github.com/shiblon/latex-makefile/blob/master/get-inputs.sed
# $(call get-inputs,<jobname>,<target>)
# TODO include for build/exam/exam.tex is wrong ...
# $(AWK) '{if ($$0 !~/^\//) { print "$2: "$$0; print $$0":"; }}'
# TODO: relative inputs need to prepend outputdir
# $(1) .fls
# $(2) document
# $(3) outputdir (cd)
# TODO: PWD is in output file!
define get-inputs
$(SED) \
-e '/^INPUT/!d' \
-e 's!^INPUT \(\./\)\{0,1\}!!' \
-e 's/[[:space:]]/\\ /g' \
-e 's/\(.*\)\.aux$$/\1.tex/' \
-e '/\.out$$/d' \
-e '/^\/dev\/null$$/d' \
-e '/^[^\/]/s!^!$(3)/!' \
-e 's!^$(CURDIR)/!!' \
'$1.fls' | $(SORT) -u | \
$(AWK) '{ print "$2: "p$$0; print p$$0":"; }'
endef

define get-bib-inputs
$(SED) -n 's!^\\bibdata{\(.*\)}$$!\1.bib!p' $(1).aux | \
$(SORT) -u | \
$(AWK) '{ print "$2: "p$$0; print p$$0":"; }'
endef

# $(1) document
# $(2) jobname
define getjobstem
$(if $(2),$(2),$(basename $(notdir $(1))))
endef

# $(1) document, required
# $(2) jobname
# $(3) builddir, if set this is used directly, else the build dir is constructed based on $(BUILDDIR), document and jobname
define getbuilddir
$(if $(3),$(3),$(BUILDDIR)/$(1)/$(call getjobstem,$(1),$(2)))
endef

# $(1) document, required
# $(2) jobname
# $(3) output
define getoutstem
$(call getbuilddir,$(1),$(2),$(3))/$(call getjobstem,$(1),$(2))
endef

# $(1) doc, required
# $(2) out, required
# $(3) jobname, can be empty
# $(4) outdir, required
# $(5) outstem, required
define pdfbuilderexp
mkdir -p $(4)
$(call run-draft-typeset,$(1),$(3),$(4))
$(AWK) '/^\\bibdata{/{b=1}END{if (b) {exit 0} else {exit 1}}' $(5).aux && \
	( $(call run-bibtex,$(5).aux) ; $(call run-draft-typeset,$(1),$(3),$(4)) ) || true
$(call run-typeset,$(1),$(3),$(4))
$(call extract-log,$(5))
$(call get-inputs,$(5),$@,$(4)) > $(BUILDDIR)/$@.d
$(call get-bib-inputs,$(5),$@,$(4)) >> $(BUILDDIR)/$@.d
touch -r $(5).pdf $(BUILDDIR)/$@.d
mkdir -p $(dir $(2))
$(MV) $(5).pdf $(2)
endef

# $(1) doc, required
# $(2) out, required
# $(3) jobname
# $(4) builddir
define pdfbuilder
$(call pdfbuilderexp,$(1),$(2),$(3),$(call getbuilddir,$(1),$(3),$(4)),$(call getoutstem,$(1),$(3),$(4)))
endef


.PHONY: $(TEXSTEM)
# explicit target list to allow tab completion in shell
(TEXSTEM): %: %.pdf

.SECONDEXPANSION:
# target for directly build documents without jobname
$(TEXPDF): $(FIGUREPDF)
# TODO: TEXPDF should depend on FIGUREPDF
$(TEXPDF) $(FIGURETEXPDF): %.pdf: %.tex $$(DEPS_$$*) $(DEPS) $(BUILDDIR)/$$@.d
	$(call pdfbuilder,$<,$@)

#$(TEXSLIDENOTEPDF): %-notes.pdf: %.tex $$(DEPS_$$*) $(DEPS) $(BUILDDIR)/$$@.d
#	$(call pdfbuilder,$<,$@,notes)

solution.pdf: tutorial.tex $$(DEPS_tutorial) $(DEPS) $(BUILDDIR)/$$@.d
	$(call pdfbuilder,$<,$@,solution)


# use hard coded for shell completion
.PHONY: figures
figures: $(FIGUREPDF) ;

# tikz figures, other figures are built by generic *.pdf target
$(FIGURETIKZPDF): %.pdf: %.tikz $$(DEPS_$$*) $(DEPS) $(BUILDDIR)/$$@.d
	$(call pdfbuilder,$<,$@)

.PHONY: all
all: $(TEXPDF) $(TEXSLIDENOTEPDF)

.PHONY : clean cleanall
clean:
	rm -fv $(DEPSMAKE) $(DEPSFIGUREMAKE)
	rm -rfv $(addprefix $(BUILDDIR)/,$(FIGURESRC) $(TEXSRC))

cleanall: clean
	rm -fv $(FIGUREPDF)
	rm -fv $(TEXPDF)
	rm -fv $(TEXSLIDENOTEPDF)
	rm -rfv $(BUILDDIR)/

$(RELEASE_FILENAME): cleanall $(TEXSRC)
	cd .. \
		&& $(FIND) "$(CURDIRBASE)" ! -path "$(CURDIRBASE)" ! -path "$(CURDIRBASE)/$@" -print0 \
		| $(TAR) -cvzf "$(CURDIRBASE)/$@" -h --null -T -

release: $(RELEASE_FILENAME)
